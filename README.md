# Attempt to measure impact of bug 15395 on performances

This repository contains a script (time-i18n.pl) that generates templates with
and without using Koha::I18N for a different number of translated strings.

Before showing my results, a few notes:
- Template cache is enabled, each template is processed once first to generate
  the cache, then it is processed a 2nd time. Only the 2nd process is timed.
- Times for templates without translation do not really represent the time
  needed to process real templates. They are basically plain text files and the
  template processing is not much more than a string concatenation, that's why
  these times are so low. Any suggestions to improve that (to have a more real
  world example) is very welcome
- The script generates templates with 10, 100, 200 and 500 different
  translatable strings. In Koha the template with the highest number of
  translatable strings is opac-tmpl/bootstrap/en/modules/ilsdi.tt and
  it contains 281 translatable strings, which are not displayed all at the same
  time (if we exclude about.tt which contains a lot of non translatable strings
  that get extracted in PO files nonetheless).
  Most templates contain less than 100 translatable strings.
  These numbers were found using this command: `grep -h '^#:' misc/translator/po/fr-FR-staff-prog.po misc/translator/po/fr-FR-opac-bootstrap.po | sort | cut -d: -f 2 | uniq -c | sort -n`
- Every test is run 10 times and the script keeps the average time

The results I got on my laptop:

```
Average for 10 strings without translation:     6.67e-05 seconds
Average for 100 strings without translation:    0.0001179 seconds
Average for 200 strings without translation:    7.84e-05 seconds
Average for 500 strings without translation:    0.000101 seconds
Average for 10 strings with translation:        0.0006516 seconds
Average for 100 strings with translation:       0.003973 seconds
Average for 200 strings with translation:       0.0068052 seconds
Average for 500 strings with translation:       0.0166518 seconds
```

## Test it yourself

To run the script, apply bug 15395, make sure PERL5LIB and KOHA\_CONF environment variables
are correctly set, and run

    perl time-i18n.pl