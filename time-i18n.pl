#!/usr/bin/env perl

use Modern::Perl;

use File::Temp qw(tempfile tempdir);
use Data::Random qw(rand_words);
use Data::Dumper;
use Template;
use Template::Constants qw(:debug);
use Time::HiRes qw(gettimeofday tv_interval);
use Test::MockModule;
use Locale::PO;
use FindBin '$Bin';
use File::Slurp qw(read_file);

use C4::Context;
use Koha::I18N; # load it here so that all required modules are compiled before we time the TT process

my $messages = Locale::PO->load_file_asarray("$Bin/po/messages.po");

my $koha_i18n = Test::MockModule->new('Koha::I18N');
$koha_i18n->mock('_base_directory', sub { "$Bin/po" });

my $c4_languages = Test::MockModule->new('C4::Languages');
$c4_languages->mock('getlanguage', sub { 'xx-XX' });

my (%averages_without, %averages_with);
for my $number_of_strings (10, 100, 200, 500) {
    my ($sum_without, $sum_with) = (0, 0);
    my $number_of_runs = 10;
    for (my $run = 1; $run <= $number_of_runs; $run++) {
        my ($time_without, $time_with) = run($number_of_strings);
        say STDERR "$number_of_strings strings, run $run:";
        say STDERR "\twithout translation:\t$time_without seconds";
        say STDERR "\twith translation:\t$time_with seconds";

        $sum_without += $time_without;
        $sum_with += $time_with;
    }
    $averages_without{$number_of_strings} = $sum_without / $number_of_runs;
    $averages_with{$number_of_strings} = $sum_with / $number_of_runs;
}

say STDERR "Summary";
foreach my $key (sort { $a <=> $b } keys %averages_without) {
    say STDERR "Average for $key strings without translation:\t" . $averages_without{$key} . " seconds";
}
foreach my $key (sort { $a <=> $b } keys %averages_with) {
    say STDERR "Average for $key strings with translation:\t" . $averages_with{$key} . " seconds";
}

sub run {
    my ($number_of_strings) = @_;

    my @phrases = map { $_->dequote($_->msgid) } @$messages[1..$number_of_strings];

    my $t = Template->new({
        INCLUDE_PATH => C4::Context->config('intrahtdocs') . '/prog/en/includes',
        PLUGIN_BASE => 'Koha::Template::Plugin',
        ENCODING => 'UTF-8',
        COMPILE_EXT => '.ttc',
        ABSOLUTE => 1,
        STAT_TTL => 60,
    });
    my $vars = {};

    my $output;

    my $tempdir = tempdir(CLEANUP => 1);

    open my $fh, '>', "$tempdir/template_without_translation.tt";
    for my $phrase (@phrases) {
        say $fh $phrase;
    }
    close $fh;

    # Process it once so it generates the cache
    $output = '';
    $t->process("$tempdir/template_without_translation.tt", $vars, \$output) or die $t->error();

    $output = '';
    my $t0 = [gettimeofday];
    $t->process("$tempdir/template_without_translation.tt", $vars, \$output) or die $t->error();
    my $time_without = tv_interval($t0);

    open $fh, '>', "$tempdir/template_with_translation.tt";
    say $fh "[% PROCESS 'i18n.inc' %]";
    for my $phrase (@phrases) {
        say $fh "[% t(\"$phrase\") %]";
    }
    close $fh;

    $output = '';
    $t->process("$tempdir/template_with_translation.tt", $vars, \$output) or die $t->error();

    $output = '';
    $t0 = [gettimeofday];
    $t->process("$tempdir/template_with_translation.tt", $vars, \$output) or die $t->error();
    my $time_with = tv_interval($t0);

    return ($time_without, $time_with);
}
