#!/usr/bin/env perl

use Modern::Perl;
use Data::Random qw(rand_words);
use Locale::PO;

my @messages;

push @messages, Locale::PO->new(
    -msgid => '',
    -msgstr =>
        "Project-Id-Version: Koha\n" .
        "PO-Revision-Date: YEAR-MO-DA HO:MI +ZONE\n" .
        "Last-Translator: FULL NAME <EMAIL\@ADDRESS>\n" .
        "Language-Team: LANGUAGE <LL\@li.org>\n" .
        "MIME-Version: 1.0\n" .
        "Content-Type: text/plain; charset=UTF-8\n" .
        "Content-Transfer-Encoding: 8bit\n"
);

for my $i (1..15000) {
    say $i;
    push @messages, Locale::PO->new(
        -msgid => (join ' ', rand_words(min => 1, max => 30)),
        -msgstr => (join ' ', rand_words(min => 1, max => 30)),
    );
}
Locale::PO->save_file_fromarray('messages.po', \@messages, 'utf8');
